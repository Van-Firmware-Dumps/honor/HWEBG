precision mediump float;
uniform sampler2D diffuseMap;

uniform int uforward;
varying vec2 v_texCoord;
varying vec4 v_color;

void main()
{
    vec2 uv = v_texCoord;

    if (uforward == 0) {
        uv.y = 1.0 - uv.y;
    }

    gl_FragColor = texture2D(diffuseMap, uv) * v_color;
}
