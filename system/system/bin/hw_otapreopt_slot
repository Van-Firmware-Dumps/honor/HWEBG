#!/system/bin/sh

# Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
# This script will move artifacts for the currently active slot.

CUR_PROD_VER=$(getprop ro.comp.hl.product_base_version.real)
DEXOPT_PROD_VER=$(getprop persist.hota.prod_version)
DO_PREOPT=$(getprop persist.hota.need_move_in_slot false)
OTA_BASE_DIR=/data/ota/b
OTA_DEST_DIR=/data/dalvik-cache
THRID_PARTY_APP_DIR=/data/app

log -p f -t otapreopt_slot otapreopt_slot "CUR_PROD_VER ${CUR_PROD_VER}"
log -p f -t otapreopt_slot otapreopt_slot "DEXOPT_PROD_VER ${DEXOPT_PROD_VER}"
log -p f -t otapreopt_slot otapreopt_slot "DO_PREOPT ${DO_PREOPT}"

if [ "$CUR_PROD_VER" == "$DEXOPT_PROD_VER" ] ; then
  PROD_VERSION_EQUAL=true
else
  PROD_VERSION_EQUAL=false
fi

log -p f -t otapreopt_slot otapreopt_slot "PROD_VERSION_EQUAL ${PROD_VERSION_EQUAL}"

if $DO_PREOPT && $PROD_VERSION_EQUAL ; then
  if [ -d "$OTA_BASE_DIR"/dalvik-cache ]; then
    log -p f -t otapreopt_slot "Moving A/B artifacts for slot b."
    OLD_SIZE=$(du -h -s $OTA_DEST_DIR/)
    NEW_SIZE=$(du -h -s $OTA_BASE_DIR/dalvik-cache)

    ota64_files=$(ls $OTA_BASE_DIR/dalvik-cache/arm64/)
    for ota64 in $ota64_files
    do
      cp -r $OTA_BASE_DIR/dalvik-cache/arm64/$ota64  $OTA_DEST_DIR/arm64/$ota64
    done;

    ota_files=$(ls $OTA_BASE_DIR/dalvik-cache/arm/)
    for ota in $ota_files
    do
      cp -r $OTA_BASE_DIR/dalvik-cache/arm64/$ota  $OTA_DEST_DIR/arm64/$ota
    done;

    log -p f -t otapreopt_slot otapreopt_slot "Moved ${NEW_SIZE} over ${OLD_SIZE}"
  else
    log -p f -t otapreopt_slot otapreopt_slot "No A/B artifacts found for slot b."
  fi
fi

log -p e -t otapreopt_slot "reset prop start"


# reset prop
$(setprop persist.hota.need_move_in_slot false)

log -p e -t otapreopt_slot "reset prop end"
